FROM node:18-buster-slim

#install tools
RUN apt update
RUN apt install -y git-core
RUN apt install -y jq
RUN apt install -y zip
RUN apt install -y gettext-base

#create alias ll
RUN echo 'alias ll="ls -l"' >> ~/.bashrc

RUN git config --global user.email "gitlab@ci.com"
RUN git config --global user.name "Gitlab CI"
